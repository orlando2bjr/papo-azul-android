package com.ed23.papoazul.app;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;

import android.os.Handler;

/**
 * Created by Orlando on 02/05/2014.
 */

public class BluetoothChatService {

    private final BluetoothAdapter mAdapter;
    // context é a Interface de Atividade
    // handler é o manipulador entre a Interface e as mensagens

    public BluetoothChatService(Context context, Handler handler)
    {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = STATE_NONE;
        mHandler = handler;

    }

    // Estados da conexão
    public static final int STATE_NONE = 0;
    public static final int STATE_LISTEN = 1;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_CONNECTED = 3;
    // membro com estado atual
    private int mState;
    public synchronized int getState() { return mState; }
    public synchronized void setState(int state)
    {
        mState = state;

    }

    private final Handler mHandler;
    private ConnectedThread mConnectedThread;
    public void write(byte[] send) {
        // Sincronizar com o segmento conectado
        ConnectedThread r;
        synchronized (this)
        {
            if(mState != STATE_CONNECTED) return;;
            r = mConnectedThread;
        }
        // gravar sem sincronizar
        r.write(send, mHandler);
    }
}
