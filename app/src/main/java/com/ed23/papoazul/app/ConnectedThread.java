package com.ed23.papoazul.app;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.os.Handler;

/**
 * Created by Orlando on 02/05/2014.
 */
public class ConnectedThread extends Thread {
    private final BluetoothSocket mmSocket;
    private final InputStream mmInStream;
    private final OutputStream mmOutStream;

    // Construtor para inicializar os soquetes
    public ConnectedThread(BluetoothSocket socket, String socketType)
    {
        mmSocket = socket;
        InputStream in = null;
        OutputStream out = null;

        // Obter os fluxos do soquete Bluetooth
        try {
            in = socket.getInputStream();
            out = socket.getOutputStream();
        }
        catch (IOException e)
        {
            Log.e("Serviço Papo Azul", "soquetes temporários não foram criados", e);
        }
        mmInStream = in;
        mmOutStream = out;
    }

    public void write(byte[] send, Handler handler) {
        try {
            mmOutStream.write(send.length);
            mmOutStream.write(send);

            // Share the sent message back to the UI Activity
            handler.obtainMessage(MainActivity.MESSAGE_WRITE, -1, -1, send)
                    .sendToTarget();
        } catch (IOException e) {
            Log.e("Serviço Papo Azul", "Exception during write", e);
        }
    }
}
